import { Component, OnInit } from '@angular/core';
import {FileItem} from "../../models/file-item";
import {CargaImagenesService} from "../../services/carga-imagenes.service";

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styleUrls: ['./carga.component.css']
})
export class CargaComponent implements OnInit {

  archivos: FileItem[] = [];
  estaSobreElemento:boolean = false;

  constructor(private cargaImagenesService:CargaImagenesService) { }

  ngOnInit(): void {
  }

  cargarImagenes() {
       this.cargaImagenesService.cargarImagenesFirebase(this.archivos);
  }

  pruebaSobreElemento(event: boolean) {
    console.log(event);

  }
}
