import {
  Directive,
  EventEmitter,
  ElementRef,
  HostListener,
  Input,
  Output
} from '@angular/core';
import {FileItem} from "../models/file-item";

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() archivos: FileItem[] = [];

  @Output() mouseSobre: EventEmitter<boolean> = new EventEmitter();

  constructor() {
  }


  @HostListener('dragover', ['$event'])
  public onDragEnter(event: any) {
    this.mouseSobre.emit(true);
    this._prevenirDetener(event);
  }

  @HostListener('dragleave', ['$event'])
  public onDragLeave(event: any) {
    this.mouseSobre.emit(false);
  }

  @HostListener('drop', ['$event'])
  public onDrop(event: any) {
    this.mouseSobre.emit(false);
    const transferencia = this._getTransferencia(event);

    if (!transferencia) {
      return;
    }

    this._extraerArchivos(transferencia.files);
    this._prevenirDetener(event);
    this.mouseSobre.emit(false);


  }

  private _prevenirDetener(event: any) {
    console.log("_prevenirDetener");
    event.preventDefault();
    event.stopPropagation();

  }

  private _getTransferencia(event: any) {
    console.log("_getTransferencia");
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer
  }

  private _extraerArchivos(archivosLista: FileList) {
    /*console.log("_extraerArchivos");
    console.log(archivosLista);*/
    for(const propiedad in Object.getOwnPropertyNames(archivosLista)){
      const archivoTemporal = archivosLista[propiedad];
      console.log("archivoTemporal--> ",archivoTemporal);
      if( this._archivoPuedeSerCargado(archivoTemporal)){
        const nuevoArchivo = new FileItem( archivoTemporal );

        this.archivos.push( nuevoArchivo );
        console.log("this.archivos", this.archivos);
      }
    }

    console.log(this.archivos);

  };



  private _archivoYaFueDroppeado(nombreArchivo: string): boolean {
    for (const archivo of this.archivos) {
      if (archivo.nombreArchivo === nombreArchivo) {
        console.log("El archivo" + nombreArchivo + "ya fue agreado")
        return true;
      }
      console.log("_archivoNoFueDroppeado");
      return false;
    }
    return false;
  }

  private _esImagen(tipoArchivo: string): boolean {
    console.log("_esImagen");
    return (tipoArchivo === '' || tipoArchivo === undefined) ? false : tipoArchivo.startsWith('image');
  }

  private _archivoPuedeSerCargado(archivo: File): boolean {
    console.log("_archivoPuedeSerCargado");
    console.log(archivo.name);
    if (this._archivoYaFueDroppeado(archivo.name) && this._esImagen(archivo.type)) {
      return true;
    } else {
      return false;
    }
  }




}
