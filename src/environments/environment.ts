// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDFojN1hTqB2r_cb-y0NuL4Zgx7xS42h04',
    authDomain: 'fir-fotos-edc47.firebaseapp.com',
    projectId: 'fir-fotos-edc47',
    storageBucket: 'fir-fotos-edc47.appspot.com',
    messagingSenderId: '992309796664',
    appId: '1:992309796664:web:cf39ab3505bd98dabf6bcd'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
